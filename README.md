## Client Setup
cd file-manager-client

npm install

npm start

Note: If you get a prompt asking you if you would like to run the React app on another port because most likely Docker is already running on port 3000, then make sure to adjust the API_CLIENT_URL variable in the .env file in the api folder to the correct port.

Note: If you are running the above command on a Windows machine and you get a prompt stating that something is already running on port 3000, modify the value for the 'start' property under 'scripts' in the package.json file to "set PORT=3002 && react-scripts start".