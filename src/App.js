import React, { useReducer, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications';
import Navbar from './components/Navbar/Navbar';
import Dashboard from './pages/Dashboard/Dashboard';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Upload from './pages/Upload/Upload';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import { 
  AuthContext, 
  reducer, 
  initialState, 
  SET_TOKEN, 
  SET_USER, 
  SET_IS_AUTHENTICATED 
} from './context/AuthContext';
import './App.css';

export default () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (localStorage.getItem('user') && localStorage.getItem('token')) {
      const token = localStorage.getItem('token');
      const user = JSON.parse(localStorage.getItem('user'));
      dispatch({ type: SET_TOKEN, payload: { token } });
      dispatch({ type: SET_USER, payload: { user } });
      dispatch({ type: SET_IS_AUTHENTICATED, payload: { isAuthenticated: true } });
    }
  }, []);
  
  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    >
      <ToastProvider>
        <Router>
          <Navbar />
          <Switch>
            <ProtectedRoute path="/" component={Dashboard} exact />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <ProtectedRoute path="/upload" component={Upload} />
          </Switch>
        </Router>
      </ToastProvider>
    </AuthContext.Provider>
  );
}
