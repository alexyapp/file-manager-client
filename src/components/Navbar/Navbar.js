import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';
import { AuthContext, LOGOUT } from '../../context/AuthContext';
import axios from 'axios';
import { FaBell } from 'react-icons/fa';

export default () => {
    const { dispatch, state } = useContext(AuthContext);
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    
    const logout = async (e) => {
        e.preventDefault();

        let response = await axios.post('/auth/logout');
        
        if (response.status === 200) {
            dispatch({ type: LOGOUT });
        }
    };
  
    return (
        <Navbar color="light" light expand="md" className="mb-4">
            <NavbarBrand tag={Link} to="/">GoReact File Manager</NavbarBrand>
            
            <NavbarToggler onClick={toggle} />

            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        {
                            state.isAuthenticated ? (
                                <UncontrolledDropdown nav inNavbar>
                                    <DropdownToggle nav caret>
                                        Hello, { state.user.name }!
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        {/* <DropdownItem divider /> */}
                                        <DropdownItem onClick={logout}>
                                            Logout
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            ) : (
                                <NavLink tag={Link} to="/login">Login / Register</NavLink>
                            )
                        }
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    );
}