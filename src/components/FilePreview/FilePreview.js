import React from 'react';

export default ({ file }) => {
    switch (file.mime_type) {
        case 'image/jpg':
        case 'image/jpeg':
            return (
                <img src={file.url} className="img-fluid" />
            )
        case 'video/mp4':
            return (
                <video width="320" height="240" controls>
                    <source src={file.url} type="video/mp4"></source>
                </video>
            );
        default:
            throw new Error('Unsupported file type.');
    }
}