import React from 'react';
import { FaEye, FaTimes } from 'react-icons/fa';
import { Row, Col } from 'reactstrap';

export default ({ 
    file, 
    pending, 
    setActiveFile, 
    setFileToDelete 
}) => {
    if (pending) {
        return (
            <>
                <Row className="mb-2 border p-3 align-items-center">
                    <Col xs={12} md={8} className="mr-auto">
                        <p className="mb-0">Name: {file.name}</p>
                    </Col>
                    <Col xs={12} md={2}>
                        <p className="mb-0">Status: Pending</p>
                    </Col>
                </Row>
            </>
        );
    }

    return (
        <Row className="mb-2 border p-3 align-items-center">
            <Col xs={12} md={8}>
                <p className="mb-0">Name: {file.name}</p>
            </Col>
            <Col xs={12} md={2} className="mr-auto">
                <p className="mb-0">Size: {file.size} KB</p>
            </Col>
            <Col xs={12} md={1}>
                <a href="#" className="mb-0" onClick={() => setActiveFile(file)}>View</a>
                {/* <FaTimes className="text-danger" onClick={setFileToDelete} style={{cursor: 'pointer'}} /> */}
            </Col>
        </Row>
    );
}