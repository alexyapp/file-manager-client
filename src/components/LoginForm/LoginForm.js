import React, { useState, useContext } from 'react';
import { 
    Button, 
    Form, 
    FormGroup, 
    Label, 
    Input,
    Container,
    Row,
    Col
} from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import { 
    AuthContext, 
    SET_TOKEN, 
    SET_USER, 
    SET_IS_AUTHENTICATED 
} from '../../context/AuthContext';

export default () => {
    const { dispatch, state } = useContext(AuthContext);
    const [form, setForm] = useState({email: '', password: ''});
    const [loading, setLoading] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);
        
        try {
            let response = await axios.post('/auth/login', form);
            const { access_token: token } = response.data;
            dispatch({ type: SET_TOKEN, payload: { token } });
            
            response = await axios.get('/auth/me');
            const { data: user } = response.data;
            dispatch({ type: SET_USER, payload: { user } });

            dispatch({ type: SET_IS_AUTHENTICATED, payload: { isAuthenticated: true } });
        } catch (error) {
            // TODO: handle error
        } finally {
            setLoading(false);
        }
    }

    const handleChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    if (state.isAuthenticated) {
        return <Redirect to="/" />
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <Label>Email</Label>
                <Input type="email" name="email" value={form.email} onChange={handleChange} />
            </FormGroup>
            <FormGroup>
                <Label for="examplePassword">Password</Label>
                <Input type="password" name="password" value={form.password} onChange={handleChange} />
            </FormGroup>

            <Container fluid>
                <Row>
                    <Col style={{ padding: 0 }}>
                        <Button disabled={loading}>
                            {loading ? 'Loading...' : 'Login'}
                        </Button>
                        <span style={{ marginLeft: '5px' }}>Don't have an account?
                            <Link to="/register" className={loading ? 'disabled' : ''}> Register</Link>
                        </span>
                    </Col>
                </Row>
            </Container>
        </Form>
    );
}