import React from 'react';

export default ({ preview, deletePreview }) => {
    switch (preview.extension) {
        case 'jpg':
        case 'jpeg':
            return (
                <div className="mb-sm-3 mb-md-0">
                    <img className="img-fluid" src={preview.src} />
                    <a href="#" onClick={e => {
                        e.preventDefault();
                        deletePreview(preview.src);
                    }} >Delete</a>
                </div>
            );
        case 'mp4':
            return (
                <video width="320" height="240" controls>
                    <source src={preview.src} type="video/mp4"></source>
                </video>
            );
        default:
            throw new Error('Unsupported file type.');
    }
}