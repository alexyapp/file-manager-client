import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

export default ({ component: Component, ...rest }) => {
    const { state } = useContext(AuthContext);

    return (
        <Route {...rest} render={props => (
            state.isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Redirect to={{
                    pathname: '/login'
                }} />
            )
        )} />
    );
}