import React, { useState } from 'react';
import { 
    Form,
    FormGroup,
    Label,
    Input, 
    Container,
    Row,
    Col,
    Button,
    Badge
} from 'reactstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { useToasts } from 'react-toast-notifications';
import UploadPreview from '../../components/UploadPreview/UploadPreview';

export default () => {
    const history = useHistory();
    const [files, setFiles] = useState([]);
    const [previews, setPreviews] = useState([]);
    const [loading, setLoading] = useState(false);
    const { addToast } = useToasts();
    const allowedFileTypes = ['jpg', 'jpeg', 'mp4'];
    let fileInput = null;

    const handleChange = e => {
        const filesToUpload = Array.from(e.target.files);

        Promise.all(filesToUpload.map(file => {
            return (new Promise((resolve,reject) => {
                let extension = file.name.split('.').pop().toLowerCase();

                if (allowedFileTypes.indexOf(extension) > -1) {
                    const reader = new FileReader();
                    reader.addEventListener('load', (e) => {
                        resolve({
                            src: e.target.result,
                            extension
                        });
                    });
                    reader.addEventListener('error', reject);
                    reader.readAsDataURL(file);
                } else {
                    // TODO: handle error
                }
            }));
        }))
        .then(images => {
            setFiles([
                ...files,
                ...filesToUpload
            ]);

            setPreviews([
                ...previews,
                ...images
            ]);
        }, error => {        
            console.error(error);
        });
    }

    const deletePreview = (file) => {
        let index = previews.map(preview => preview.src).indexOf(file);
        let filteredFiles = files.filter((file, i) => i !== index);
        let filteredPreviews = previews.filter(preview => preview.src !== file);

        setFiles(filteredFiles);
        setPreviews(filteredPreviews);
    }

    const upload = async () => {
        setLoading(true);

        const formData = new FormData();

        for (const file of files) {
            formData.append('files[]', file);
        }

        try {
            let response = await axios.post('/files', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });

            if (response.status === 202) {
                addToast(response.data.message, {
                    appearance: 'success',
                    autoDismiss: true
                });

                history.push('/');
            }
        } catch (error) {
            // TODO: handle error
        } finally {
            setLoading(false);
        }
    }

    return (
        <Container>
            <Row>
                <Col>
                    <Form>
                        <FormGroup>
                            <Button disabled={loading} onClick={() => fileInput.click()} color="primary">
                                Choose Files <Badge color="light">{previews.length > 0 && previews.length}</Badge>
                            </Button>
                            <Input className="d-none" innerRef={input => { fileInput = input }} type="file" multiple onChange={handleChange} accept="image/jpg,image/jpeg,video/mp4" />

                            {
                                previews.length > 0 && (
                                    <Button className="ml-2" disabled={loading} onClick={upload} color="primary">
                                        {
                                            loading ? `Uploading ${previews.length} file(s)...` : `Upload ${previews.length} file(s)`
                                        }
                                    </Button>
                                )
                            }
                        </FormGroup>
                    </Form>
                </Col>
            </Row>

            {
                previews && (
                    <Row>
                        {previews.map(preview => (
                            <Col md={3} key={Math.random()}>
                                <UploadPreview preview={preview} deletePreview={deletePreview} />
                            </Col>
                        ))}
                    </Row>
                )
            }
        </Container>
    );
}