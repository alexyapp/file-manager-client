import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { 
    Container,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button
} from 'reactstrap';
import File from '../File/File';
import FilePreview from '../FilePreview/FilePreview';
import Loading from '../Loading/Loading';

export default () => {
    const [files, setFiles] = useState([]);
    const [pendingFiles, setPendingFiles] = useState([]);
    const [activeFile, setActiveFile] = useState(null);
    const [fileToDelete, setFileToDelete] = useState(null);
    const [loading, setLoading] = useState(false);
    const [modal, setModal] = useState(false);

    useEffect(() => {
        setLoading(true);
        
        const getFiles = axios.get('/files');
        const getPendingFiles = axios.get('/files?pending=true');

        Promise.all([getFiles, getPendingFiles])
            .then(([getFilesResponse, getPendingFilesResponse]) => {
                if (getFilesResponse.status === 200) {
                    const { data: files } = getFilesResponse.data;
    
                    setFiles([
                        ...files,
                        files
                    ]);
                }
    
                if (getPendingFilesResponse.status === 200) {
                    const { data: pendingFiles } = getPendingFilesResponse.data;
    
                    setPendingFiles([
                        ...pendingFiles,
                        pendingFiles
                    ]);
                }
            })
            .catch(error => {
                // TODO: handle error
            })
            .finally(() => setLoading(false));
    }, []);

    useEffect(() => {
        setModal(true);
    }, [activeFile]);

    const toggle = () => setModal(!modal);

    return (
        <>
            {
                loading ? (
                    <Loading />
                ) : (
                    <>
                        <Container className="mb-5" fluid>
                            <h4 style={{ margin: '0 -15px 1rem -15px' }}>Pending</h4>
                            {pendingFiles.map(file => {
                                if (file.name) {
                                    return <File key={Math.random()} file={file} pending={true} setActiveFile={setActiveFile} />;
                                }
                            })}
                        </Container>

                        <Container fluid>
                            <h4  style={{ margin: '0 -15px 1rem -15px' }}>Available</h4>
                            {files.map(file => {
                                if (file.name) {
                                    return <File key={Math.random()} file={file} setActiveFile={setActiveFile} />;
                                }
                            })}
                        </Container>

                        {
                            activeFile && (
                                <Modal isOpen={modal} toggle={toggle}>
                                    <ModalBody className="text-center">
                                        <FilePreview file={activeFile} />
                                    </ModalBody>
                                </Modal>
                            )
                        }
                    </>
                )
            }
        </>
    );
}