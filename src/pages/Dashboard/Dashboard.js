import React from 'react';
import { Container, Col, Row, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import FileList from '../../components/FileList/FileList';
import { FaPlus } from 'react-icons/fa';

export default () => (
    <Container className="pb-4">
        <Row>
            <Col>
                <div className="d-flex align-items-center mb-4">
                    <h2 className="mb-0 mr-auto">Dashboard</h2>

                    <Link to="/upload">
                        <FaPlus style={{ fontSize: '1.5rem' }} />
                    </Link>
                </div>

                <FileList />

                <Button className="mt-4" tag={Link} to="/upload" color="primary">Upload</Button>
            </Col>
        </Row>
    </Container>
)