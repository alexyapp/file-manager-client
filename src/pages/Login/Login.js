import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import LoginForm from '../../components/LoginForm/LoginForm';

export default () => (
    <div>
        <Container>
            <Row>
                <Col md={{ size: 6, offset: 3 }}>
                    <h2>Login</h2>
                    <LoginForm />
                </Col>
            </Row>
        </Container>
    </div>
);