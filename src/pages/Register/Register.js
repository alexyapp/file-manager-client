import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import RegisterForm from '../../components/RegisterForm/RegisterForm';

export default () => (
    <Container>
        <Row>
            <Col md={{ size: 6, offset: 3 }}>
                <h2>Register</h2>
                <RegisterForm />
            </Col>
        </Row>
    </Container>
);