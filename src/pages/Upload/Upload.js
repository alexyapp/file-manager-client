import React from 'react';
import UploadForm from '../../components/UploadForm/UploadForm';
import { Container, Row, Col } from 'reactstrap';

export default () => {
    return (
        <div>
            <Container className="mb-2">
                <Row>
                    <Col>
                        <h2>Upload</h2>
                    </Col>
                </Row>
            </Container>

            <UploadForm />
        </div>
    );
}