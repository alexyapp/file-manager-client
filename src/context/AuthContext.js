import { createContext } from 'react';

export const SET_TOKEN = 'SET_TOKEN';
export const SET_USER = 'SET_USER';
export const SET_IS_AUTHENTICATED = 'SET_IS_AUTHENTICATED';
export const LOGOUT = 'LOGOUT';

export const AuthContext = createContext();

export const initialState = {
    isAuthenticated: false,
    user: null,
    token: null,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case "SET_TOKEN":
        const { token } = action.payload;

        localStorage.setItem("token", token);
        return {
            ...state,
            token
        };
    case "SET_USER":
        const { user } = action.payload;

        localStorage.setItem("user", JSON.stringify(user));
        return {
            ...state,
            user: user
        };
    case "SET_IS_AUTHENTICATED":
        const { isAuthenticated } = action.payload;

        return {
            ...state,
            isAuthenticated
        }
    case "LOGOUT":
        localStorage.clear();

        return {
            ...state,
            isAuthenticated: false,
            user: null,
            token: null
        };
    default:
      return state;
  }
};